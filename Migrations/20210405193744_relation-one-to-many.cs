﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactManagement.Migrations
{
    public partial class relationonetomany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PhoneNumber_Contact_ContactID",
                table: "PhoneNumber");

            migrationBuilder.AlterColumn<int>(
                name: "ContactID",
                table: "PhoneNumber",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_Contact_ContactID",
                table: "PhoneNumber",
                column: "ContactID",
                principalTable: "Contact",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PhoneNumber_Contact_ContactID",
                table: "PhoneNumber");

            migrationBuilder.AlterColumn<int>(
                name: "ContactID",
                table: "PhoneNumber",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_Contact_ContactID",
                table: "PhoneNumber",
                column: "ContactID",
                principalTable: "Contact",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
