﻿using Microsoft.EntityFrameworkCore;
using ContactManagement.Models;

namespace ContactManagement.Data
{
    public class ContactManagementContext : DbContext
    {
        public ContactManagementContext(DbContextOptions<ContactManagementContext> options)
            : base(options)
        {
        }

        public DbSet<Contact> Contact { get; set; }
        public DbSet<PhoneNumber> PhoneNumber { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PhoneNumber>()
                .HasOne<Contact>(c => c.Contact)
                .WithMany(p => p.PhoneNumbers)
                .HasForeignKey(s => s.ContactID);
        }
    }
}
