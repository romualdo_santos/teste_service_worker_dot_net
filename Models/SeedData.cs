﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ContactManagement.Data;
using System;
using System.Linq;


namespace ContactManagement.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ContactManagementContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ContactManagementContext>>()))
            {
                if (context.Contact.Any())
                {
                    return;   // DB has been seeded
                }

                context.Contact.AddRange(
                    new Contact
                    {
                        Name = "Romualdo",
                        LastName = "Santos",
                        Address = "Salvador-BA",
                        Email = "romualdo@somemail.com",
                        Birthday = DateTime.Parse("1994-27-1")
                    },

                    new Contact
                    {
                        Name = "Romuboy",                        
                        Address = "??????",
                        Email = "romuboy@somemail.com",                        
                    },

                    new Contact
                    {
                        Name = "Sonic",
                        LastName = "the Hedgehog",
                        Address = "Green Hill Zone",
                        Email = "sonic@sega.com",
                        Birthday = DateTime.Parse("1991-23-6")
                    }
                );
                context.SaveChanges();

                context.PhoneNumber.AddRange(
                    new PhoneNumber
                    {
                        ContactID = 1,
                        Number = "71988888888",
                    },

                    new PhoneNumber
                    {
                        ContactID = 1,
                        Number = "71988888880",
                    },

                    new PhoneNumber
                    {
                        ContactID = 1,
                        Number = "71988888808",
                    },

                    new PhoneNumber
                    {
                        ContactID = 2,
                        Number = "71988888088",
                    }                    
                );
                context.SaveChanges();
            }
        }
    }
}
