﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactManagement.Models
{
    public class PhoneNumber
    {
        public int ID { get; set; }        
        public string Number { get; set; }

        public int? ContactID { get; set; }
        public Contact Contact { get; set; }
    }
}
