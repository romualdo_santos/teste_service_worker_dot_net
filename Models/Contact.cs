﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactManagement.Models
{
    public class Contact
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Sobrenome")]
        public string LastName { get; set; }

        [Display(Name = "Endereço")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Data de aniversário"), DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [Display(Name = "Telefones")]
        public virtual ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}